# Natoures Travel Agency 
This was my itteration in the first project of Advanced CSS & SASS from jonas.io! 

**Visit Demo on netlify** - [click here!](https://natoures-learn.netlify.com/)

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

What things you need to install the software and how to install them

```
NPM AND SASS  AND  LIVE-SERVER INSTALLED ON YOUR MACHINE
```

### Installing

A step by step series of examples that tell you how to get a development env running

```
npm install 
```


## Running 
For building your css file and starting the live-reload you need to run 

```
npm run sass
```



## Coding Style 

This project is build with Dekstop-first in mind. 
All the markup and classname is based on BEM.
For the styling is based on CSS and 7-1 architecture.



## Authors

All Design copyrights belongs to JONAS SCHMEIDTMANN [JONAS.IO](jonas.io)

Karanikolas Konstantinos - [blog](https:/karanikolas.work)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

